import React, { useState, useEffect } from 'react'
import HomeScreen from './HomeScreen'
import GameScreen from './GameScreen'
import { initConnection } from './connection'
import Storage, { EventType } from './Storage'
import config from './config'

export default function App(): JSX.Element {
  const [ready, setReady] = useState(false)

  useEffect(() => {
    if (ready) {
      if (config.offline) {
        Storage.eventEmitter.emit(EventType.PlaySet)
      } else {
        initConnection()
      }
    }
  }, [ready])

  const handleConnect = (): void => {
    setReady(true)
  }

  return ready ? <GameScreen /> : <HomeScreen onConnect={handleConnect} />
}
