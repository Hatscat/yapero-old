import React from 'react'
import GameScreenControls from './GameScreenControls'
import Storage from './Storage'

export type Props = {}

export default function GameScreen(props: Props): JSX.Element {
  const canvasSize = Storage.gameCanvas.size

  const handleCanvasClick = (
    event: React.MouseEvent<HTMLCanvasElement, MouseEvent>
  ): void => {
    Storage.eventEmitter.emit('GameCanvasClicked', {
      x: event.clientX,
      y: event.clientY,
    })
  }

  return (
    <>
      <canvas
        ref={(element) => {
          Storage.gameCanvas.element = element
        }}
        width={canvasSize.width}
        height={canvasSize.height}
        onClick={handleCanvasClick}
      />
      <GameScreenControls
        onGameChange={(game) => {
          Storage.currentGame = game
        }}
      />
    </>
  )
}
