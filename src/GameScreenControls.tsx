import React, { useState } from 'react'
import {
  BottomNavigation,
  BottomNavigationAction,
  Grid,
  IconButton,
} from '@material-ui/core'
import StyleIcon from '@material-ui/icons/Style'
import EditIcon from '@material-ui/icons/Edit'
import VoiceChatIcon from '@material-ui/icons/VoiceChat'
import VideocamOff from '@material-ui/icons/VideocamOff'
import MicOff from '@material-ui/icons/MicOff'

export enum Game {
  None,
  Pictionary,
  Set,
}

export type Props = {
  onGameChange: (game: Game) => void
}

export default function GameScreenControls(props: Props): JSX.Element {
  const [currentGame, setCurrentGame] = useState<Game>(Game.None)

  const handleGameChange = (event: React.ChangeEvent<{}>, game: Game): void => {
    setCurrentGame(game)
    props.onGameChange(game)
  }

  return (
    <Grid
      container
      direction="row"
      justify="space-between"
      alignItems="center"
      style={{ position: 'fixed', bottom: 0 }}
    >
      <Grid item>
        <BottomNavigation value={currentGame} onChange={handleGameChange}>
          <BottomNavigationAction
            label="Chat"
            value={Game.None}
            icon={<VoiceChatIcon />}
          />
          <BottomNavigationAction
            label="Pictionary"
            value={Game.Pictionary}
            icon={<EditIcon />}
          />
          <BottomNavigationAction
            label="Set"
            value={Game.Set}
            icon={<StyleIcon />}
          />
        </BottomNavigation>
      </Grid>
      <Grid item>
        <IconButton>
          <MicOff />
        </IconButton>
        <IconButton>
          <VideocamOff />
        </IconButton>
      </Grid>
    </Grid>
  )
}
