import React, { useState } from 'react'
import {
  Box,
  Container,
  Grid,
  Typography,
  Button,
  TextField,
} from '@material-ui/core'
import Storage from './Storage'
import theme from './theme'

export type Props = {
  onConnect: () => void
}

export default function HomeScreen(props: Props): JSX.Element {
  const handleReady = (userName: string): void => {
    Storage.userName = userName
    props.onConnect()
  }

  const handleVideo = (video: HTMLVideoElement | null): void => {
    if (!video) {
      return
    }

    navigator.mediaDevices
      .getUserMedia({
        video: true,
        audio: true,
      })
      // eslint-disable-next-line promise/prefer-await-to-then
      .then((stream) => {
        video.srcObject = stream

        return stream
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error)
      })
  }

  return (
    <Container>
      <Box my={4}>
        <Typography variant="h4" component="h1" align="center">
          Yapero
        </Typography>

        <video
          autoPlay
          playsInline
          muted
          ref={handleVideo}
          style={{
            display: 'block',
            margin: 'auto',
            marginTop: theme.spacing(4),
            marginBottom: theme.spacing(4),
          }}
        />

        <NameForm onReady={handleReady} />
      </Box>
    </Container>
  )
}

function NameForm(props: { onReady: (userName: string) => void }): JSX.Element {
  const [userName, setUserName] = useState<string>(Storage.userName)

  const handleUserNameChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ): void => {
    const { value } = event.target
    setUserName(value)
  }

  const handleKeyPress = (
    event: React.KeyboardEvent<HTMLInputElement>
  ): void => {
    if (event.key === 'Enter' && userName.length > 3) {
      props.onReady(userName)
    }
  }

  return (
    <Grid container direction="row" justify="center" alignItems="center">
      <TextField
        variant="outlined"
        size="small"
        label="Name"
        autoFocus
        value={userName}
        onChange={handleUserNameChange}
        onKeyPress={handleKeyPress}
      />

      <Button
        variant="contained"
        size="large"
        color="primary"
        disabled={userName.length < 3}
        onClick={() => props.onReady(userName)}
      >
        Connect
      </Button>
    </Grid>
  )
}
