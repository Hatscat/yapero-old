import { EventEmitter } from 'events'
import { DataConnection } from 'peerjs'

export enum Game {
  None,
  Pictionary,
  Set,
}

export enum EventType {
  SlaveAction = 'slaveAction',
  MasterAction = 'masterAction',
  PlaySet = 'playSet',
}

export type ConnectionRole = 'master' | 'slave'

export type GameCanvas = {
  buffer: HTMLCanvasElement
  bufferCtx: CanvasRenderingContext2D
  element: HTMLCanvasElement | null
  ctx: CanvasRenderingContext2D | null
  size: { width: number; height: number }
}

const gameBuffer = document.createElement('canvas')

const state = {
  userName: '',
  currentGame: Game.None,
  connectionRole: 'master',
  connectionId: '',
  dataConnectionWithMaster: null as DataConnection | null,
  dataConnectionWithSlaves: [] as DataConnection[],
  eventEmitter: new EventEmitter(),
  gameCanvasElement: null as GameCanvas['element'],
  gameCanvasCtx: null as GameCanvas['ctx'],
  gameBuffer,
  gameBufferCtx: gameBuffer.getContext('2d') as CanvasRenderingContext2D,
}

const gameCanvas = {
  get buffer() {
    return state.gameBuffer
  },
  get bufferCtx() {
    return state.gameBufferCtx
  },
  get element() {
    return state.gameCanvasElement
  },
  set element(canvas: HTMLCanvasElement | null) {
    state.gameCanvasElement = canvas
    state.gameCanvasCtx = canvas?.getContext('2d') || null
    Object.assign(state.gameBuffer, gameCanvas.size)
  },
  get ctx() {
    return state.gameCanvasCtx
  },
  get size() {
    return { width: innerWidth, height: innerHeight - 64 }
  },
} as const

const Storage = {
  get userName() {
    return localStorage.getItem('yapero:username') || ''
  },
  set userName(name: string) {
    localStorage.setItem('yapero:username', name)
  },
  get currentGame() {
    return state.currentGame
  },
  set currentGame(game: Game) {
    state.currentGame = game
  },
  get connectionRole() {
    return state.connectionRole as ConnectionRole
  },
  set connectionRole(role: ConnectionRole) {
    state.connectionRole = role
  },
  get isMaster() {
    return Storage.connectionRole === 'master'
  },
  get connectionId() {
    return state.connectionId
  },
  set connectionId(id: string) {
    state.connectionId = id
  },
  get dataConnectionWithMaster() {
    return state.dataConnectionWithMaster
  },
  set dataConnectionWithMaster(connection: DataConnection | null) {
    state.dataConnectionWithMaster = connection
  },
  get dataConnectionWithSlaves() {
    return state.dataConnectionWithSlaves
  },
  get eventEmitter() {
    return state.eventEmitter
  },
  get gameCanvas(): GameCanvas {
    return gameCanvas
  },
} as const

export default Storage
