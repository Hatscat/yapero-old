const config = {
  get offline() {
    return /[#&]offline=1/i.test(location.hash)
  },
}

export default config
