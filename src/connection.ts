/* eslint-disable no-console */
import Peer from 'peerjs'
import Storage, { EventType } from './Storage'

export function initConnection(): void {
  if (Storage.isMaster) {
    initMaster()
  } else {
    initSlave()
  }
}

function initMaster(): void {
  const peer = new Peer(Storage.connectionId, { debug: 2 })

  peer.on('open', (id) => {
    console.log(`I'm master, and my peer ID is: ${id}`)
    Storage.eventEmitter.emit(EventType.PlaySet)

    peer.on('connection', (dataConnectionWithSlave) => {
      console.log('here is a new slave:', dataConnectionWithSlave)
      Storage.dataConnectionWithSlaves.push(dataConnectionWithSlave)

      dataConnectionWithSlave.on('open', () => {
        // Receive messages
        dataConnectionWithSlave.on('data', (data: unknown) => {
          console.log(`Received from slave "${id}": `, data)
          Storage.eventEmitter.emit(
            EventType.SlaveAction,
            data,
            dataConnectionWithSlave
          )
        })

        // Send messages
        dataConnectionWithSlave.send('Hello slave...')
      })

      dataConnectionWithSlave.on('close', () => {
        Storage.dataConnectionWithSlaves.splice(
          Storage.dataConnectionWithSlaves.findIndex(
            (co) => co.label === dataConnectionWithSlave.label
          ),
          1
        )
      })
    })
  })
}

function initSlave(): void {
  const peer = new Peer(undefined, { debug: 2 })

  peer.on('open', (id) => {
    console.log(`I'm a slave, and my peer ID is: ${id}`)
    Storage.eventEmitter.emit(EventType.PlaySet)

    const dataConnectionWithMaster = peer.connect(Storage.connectionId, {
      metadata: {
        userName: Storage.userName,
      },
    })

    dataConnectionWithMaster.on('open', () => {
      Storage.dataConnectionWithMaster = dataConnectionWithMaster
      // Receive messages
      dataConnectionWithMaster.on('data', (data: unknown) => {
        console.log('Received from Master:', data)
        Storage.eventEmitter.emit(
          EventType.MasterAction,
          data,
          dataConnectionWithMaster
        )
      })

      // Send messages
      dataConnectionWithMaster.send('Hello Master!')
    })
  })

  peer.on('error', (error) => {
    console.error('Peer connection to master issue:', error)
    if (/could not connect/i.test(error)) {
      // slave become master
      Storage.connectionRole = 'master'
      peer.destroy()
      initConnection()
    }
  })
}
