import React from 'react'
import ReactDOM from 'react-dom'
import { CssBaseline } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/core/styles'
import { v4 as uuid } from 'uuid'
import theme from './theme'
import App from './App'
import { init as initSetGame } from './setGame'
import Storage, { EventType } from './Storage'

addEventListener('load', main)

function main(): void {
  renderApp()
  setupGameEvents()
  setupConnectionInfos()
}

function renderApp(): void {
  ReactDOM.render(
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>,
    document.querySelector('#root')
  )
}

function setupConnectionInfos(): void {
  const idRegex = /[#&]+id=([^&,;]+)/
  const idMatches = location.hash.match(idRegex)
  if (idMatches) {
    Storage.connectionRole = 'slave'
    Storage.connectionId = idMatches[1]
  } else {
    const id = uuid()
    Storage.connectionRole = 'master'
    Storage.connectionId = id
    location.hash += `#id=${id}`
  }
}

function setupGameEvents(): void {
  window.addEventListener('resize', () => {
    const { element, size, buffer } = Storage.gameCanvas
    if (element) {
      Object.assign(element, size)
    }
    Object.assign(buffer, size)
  })

  Storage.eventEmitter.on(EventType.PlaySet, initSetGame)
}
