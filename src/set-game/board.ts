import { Card, isCardSetValid, getValidSetCount } from './card'
import { drawCardsWithASetGivenBoardCards } from './stack'

export type Board = {
  cards: Card[]
  setCount: number
}

export type DrawResult = {
  cards: Card[]
  setCount: number
}

// TODO: TO TEST!
export { getBoard, hereIsMySet }

const BOARD_SIZE = 3 * 4

const boardCards: Card[] = []

function getBoard(): Board {
  if (boardCards.length < BOARD_SIZE) {
    boardCards.push(
      ...drawCardsWithASetGivenBoardCards(
        BOARD_SIZE - boardCards.length,
        boardCards
      )
    )
  }

  return {
    cards: boardCards,
    setCount: getValidSetCount(boardCards),
  }
}

function hereIsMySet(cardIndices: [number, number, number]): DrawResult | null {
  if (
    isCardSetValid(
      boardCards[cardIndices[0]],
      boardCards[cardIndices[1]],
      boardCards[cardIndices[2]]
    )
  ) {
    const newCards = drawCardsWithASetGivenBoardCards(
      3,
      boardCards.filter((card, cardIdx) => !cardIndices.includes(cardIdx))
    )

    cardIndices.forEach((cardIdx, i) => {
      boardCards.splice(cardIdx, 1, newCards[i])
    })

    return {
      cards: newCards,
      setCount: getValidSetCount(boardCards),
    }
  }

  return null
}
