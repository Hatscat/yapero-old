import {
  CardColor,
  CardNumber,
  CardShading,
  CardShape,
  createCard,
  createAllPossibleCards,
  getMissingCard,
  isCardsEqual,
  isCardSetValid,
  getValidSetCount,
} from './card'

describe('card', () => {
  describe('getMissingCard()', () => {
    test('should find a valid Set', () => {
      const cardA = createCard({
        number: CardNumber.Three,
        color: CardColor.Purple,
        shape: CardShape.Oval,
        shading: CardShading.Striped,
      })
      const cardB = createCard({
        number: CardNumber.Three,
        color: CardColor.Red,
        shape: CardShape.Oval,
        shading: CardShading.Open,
      })
      const cardC = getMissingCard(cardA, cardB)

      expect(cardC).toEqual({
        number: CardNumber.Three,
        color: CardColor.Green,
        shape: CardShape.Oval,
        shading: CardShading.Solid,
      })

      expect(isCardSetValid(cardA, cardB, cardC)).toBe(true)
    })
  })

  describe('isSetValid()', () => {
    test('should be a valid Set', () => {
      expect(isCardSetValid(createCard(), createCard(), createCard())).toBe(
        true
      )
    })

    test('should be an invalid Set', () => {
      expect(
        isCardSetValid(
          createCard({ number: CardNumber.One }),
          createCard({ number: CardNumber.One }),
          createCard({ number: CardNumber.Two })
        )
      ).toBe(false)
    })
  })

  describe('createAllPossibleCards()', () => {
    test('should return unique cards', () => {
      const cards = createAllPossibleCards()

      expect(cards.length).toBe(81)

      const uniqueCards = cards.filter(
        (card, index, self) =>
          self.findIndex((otherCard) => isCardsEqual(card, otherCard)) === index
      )

      expect(uniqueCards.length).toBe(cards.length)
    })
  })

  describe('getValidSetCount()', () => {
    test('should return 0', () => {
      const cardsSet = [
        createCard({
          number: CardNumber.One,
          color: CardColor.Green,
          shape: CardShape.Squiggle,
          shading: CardShading.Open,
        }),
        createCard({
          number: CardNumber.Two,
          color: CardColor.Red,
          shape: CardShape.Diamond,
          shading: CardShading.Open,
        }),
        createCard({
          number: CardNumber.Three,
          color: CardColor.Green,
          shape: CardShape.Oval,
          shading: CardShading.Open,
        }),
      ]
      expect(getValidSetCount(cardsSet)).toBe(0)
    })

    test('should return 1', () => {
      const cardsSet = [
        createCard({
          number: CardNumber.One,
          color: CardColor.Green,
          shape: CardShape.Squiggle,
          shading: CardShading.Open,
        }),
        createCard({
          number: CardNumber.Two,
          color: CardColor.Green,
          shape: CardShape.Diamond,
          shading: CardShading.Open,
        }),
        createCard({
          number: CardNumber.Three,
          color: CardColor.Green,
          shape: CardShape.Oval,
          shading: CardShading.Open,
        }),
      ]
      expect(getValidSetCount(cardsSet)).toBe(1)
    })

    test('should return 2', () => {
      const cardsSet = [
        createCard({
          number: CardNumber.One,
          color: CardColor.Green,
          shape: CardShape.Squiggle,
          shading: CardShading.Open,
        }),
        createCard({
          number: CardNumber.Two,
          color: CardColor.Green,
          shape: CardShape.Diamond,
          shading: CardShading.Open,
        }),
        createCard({
          number: CardNumber.Three,
          color: CardColor.Green,
          shape: CardShape.Oval,
          shading: CardShading.Open,
        }),
        createCard({
          number: CardNumber.Three,
          color: CardColor.Red,
          shape: CardShape.Oval,
          shading: CardShading.Solid,
        }),
        createCard({
          number: CardNumber.Three,
          color: CardColor.Purple,
          shape: CardShape.Oval,
          shading: CardShading.Striped,
        }),
        createCard({
          number: CardNumber.Two,
          color: CardColor.Purple,
          shape: CardShape.Oval,
          shading: CardShading.Solid,
        }),
      ]
      expect(getValidSetCount(cardsSet)).toBe(2)
    })

    test('should return all unique cards set count', () => {
      const allCards = createAllPossibleCards()
      expect(getValidSetCount(allCards)).toBe(1080)
    })
  })
})
