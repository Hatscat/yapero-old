export enum CardNumber {
  One = 1,
  Two = 2,
  Three = 3,
}

export enum CardColor {
  Red = 1,
  Green = 2,
  Purple = 3,
}

export enum CardShape {
  Diamond = 1,
  Squiggle = 2,
  Oval = 3,
}

export enum CardShading {
  Solid = 1,
  Striped = 2,
  Open = 3,
}

export type Card = {
  number: CardNumber
  color: CardColor
  shape: CardShape
  shading: CardShading
}

export {
  createCard,
  createAllPossibleCards,
  getMissingCard,
  isCardsEqual,
  isCardSetValid,
  getValidSetCount,
}

function createCard(props?: Partial<Card>): Card {
  return {
    number: CardNumber.One,
    color: CardColor.Red,
    shape: CardShape.Diamond,
    shading: CardShading.Solid,
    ...props,
  }
}

function createAllPossibleCards(): Card[] {
  const clampPropValue = (n: number): number => Math.floor(n % 3) + 1

  return Array.from(Array(3 ** 4)).map((_, i) => ({
    number: clampPropValue(i),
    color: clampPropValue(i / 3),
    shape: clampPropValue(i / 3 ** 2),
    shading: clampPropValue(i / 3 ** 3),
  }))
}

function getMissingCardPropertyValue<Property extends keyof Card>(
  property: Property,
  cardA: Card,
  cardB: Card
): Card[Property] {
  return cardA[property] === cardB[property]
    ? cardA[property]
    : ((1 + 2 + 3 - (cardA[property] + cardB[property])) as Card[Property])
}

function getMissingCard(cardA: Card, cardB: Card): Card {
  return (Object.keys(cardA) as (keyof Card)[]).reduce(
    (cardProps: Partial<Card>, property) =>
      Object.assign(cardProps, {
        [property]: getMissingCardPropertyValue(property, cardA, cardB),
      }),
    {}
  ) as Card
}

function isCardsEqual(cardA: Card, cardB: Card): boolean {
  return (
    cardA.number === cardB.number &&
    cardA.color === cardB.color &&
    cardA.shape === cardB.shape &&
    cardA.shading === cardB.shading
  )
}

function isCardSetValid(cardA: Card, cardB: Card, cardC: Card): boolean {
  const correctCardC = getMissingCard(cardA, cardB)

  return isCardsEqual(cardC, correctCardC)
}

function getValidSetCount(cards: readonly Card[]): number {
  return cards.reduce((totalCount, cardA, indexA, arrayA) => {
    return (
      totalCount +
      arrayA.slice(indexA).reduce((count, cardB, indexB, arrayB) => {
        const expectedCardC = getMissingCard(cardA, cardB)
        const cardC = arrayB
          .slice(indexB + 1)
          .find((c) => isCardsEqual(c, expectedCardC))

        return count + (cardC ? 1 : 0)
      }, 0)
    )
  }, 0)
}
