import { Card, createAllPossibleCards, getValidSetCount } from './card'

// TODO: TO TEST!
export { drawCards, drawCardsWithASetGivenBoardCards, discardCards }

const stack: Card[] = []
const discard: Card[] = []

function drawCards(count: number): Card[] {
  if (count < 1) {
    /** Prevent mistakes */
    return []
  }

  if (stack.length >= count) {
    /** Draw cards from stack */
    return count === 1
      ? [stack.pop() as Card]
      : Array.from(Array(count)).map((_) => stack.pop() as Card)
  }

  if (discard.length + stack.length >= count) {
    /** Shuffle the discard cards and fill the stack with them */
    shuffleArray(discard)
    stack.push(...discard.splice(0))

    return drawCards(count)
  }

  /** Fill the stack with new cards, shuffle it, and empty the discard */
  stack.push(...createAllPossibleCards())
  shuffleArray(stack)
  discard.splice(0)

  return drawCards(count)
}

function drawCardsWithASetGivenBoardCards(
  count: number,
  boardCards: readonly Card[]
): Card[] {
  const drewCards = drawCards(count)

  if (count < 3 || getValidSetCount(boardCards.concat(drewCards)) > 0) {
    /** Nothing special, simple draw */
    return drewCards
  }

  /** Temporary buffer to keep already tested cards to find a valid set */
  const testedCards: Card[] = []
  do {
    /** Draw a new card first (to avoid issue when the stack is empty) */
    const newCard = drawCards(1)[0]
    /** Drop a drew card to the end of the stack */
    testedCards.push(drewCards.pop() as Card)
    /** Put the new card to replace the dropped one */
    drewCards.unshift(newCard)
  } while (getValidSetCount(boardCards.concat(drewCards)) < 1)

  /** (re)Place tested Card to the end of the stack */
  stack.unshift(...testedCards)

  return drewCards
}

function discardCards(cards: Card[]): void {
  discard.push(...cards)
}

function shuffleArray(array: unknown[]): void {
  /** shuffle algorithm from https://en.wikipedia.org/wiki/Fisher-Yates_shuffle */
  for (let i = array.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[array[i], array[j]] = [array[j], array[i]]
  }
}
