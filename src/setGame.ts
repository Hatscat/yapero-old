import { Rectangle, isPointInsideRectangle } from './utils'
import { getBoard, hereIsMySet } from './set-game'
import Storage, { EventType, Game } from './Storage'
import { Card, CardColor, CardShading, CardShape } from './set-game/card'

export { init, stop }

type CardInfo = Card & {
  rectangle: Rectangle
}

const gameState = {
  cards: [...Array(12)].map((_) => ({})) as CardInfo[],
  boardLayout: {} as Rectangle,
  cardsDisposal: {} as Resolution,
  setCount: 0,
  selectedCardIndices: [] as number[],
}

function init(): void {
  const { element } = Storage.gameCanvas
  window.addEventListener('resize', resizeHandler)
  if (element) {
    element.addEventListener('click', clickHandler)
  }
  refresh()
}

function stop(): void {
  const { element } = Storage.gameCanvas
  window.removeEventListener('resize', resizeHandler)
  if (element) {
    element.addEventListener('click', clickHandler)
  }
}

function resizeHandler(): void {
  refresh()
}

function clickHandler(event: MouseEvent): void {
  const pointer = { x: event.clientX, y: event.clientY }
  const selectedCardIndex = gameState.cards.findIndex((cardInfo) => {
    return isPointInsideRectangle(pointer, cardInfo.rectangle)
  })

  if (selectedCardIndex === -1) {
    return undefined
  }

  const existingIndexOfIndex = gameState.selectedCardIndices.indexOf(
    selectedCardIndex
  )

  if (existingIndexOfIndex === -1) {
    if (Storage.isMaster) {
      Storage.eventEmitter.emit(EventType.MasterAction, { game: Game.Set })
    } else {
      Storage.eventEmitter.emit(EventType.SlaveAction, {
        game: Game.Set,
        action: 'SELECT_CARD',
        cardIndex: selectedCardIndex,
      })
    }
    gameState.selectedCardIndices.push(selectedCardIndex)

    if (gameState.selectedCardIndices.length === 3) {
      // Set !
      const setCardIndices = gameState.selectedCardIndices.splice(0) as [
        number,
        number,
        number
      ]
      const setResult = hereIsMySet(setCardIndices)
      // eslint-disable-next-line no-console
      console.log('>>> SET!', setResult)

      return refresh()
    }
  } else {
    gameState.selectedCardIndices.splice(existingIndexOfIndex, 1)
  }

  return render()
}

type Resolution = {
  w: number
  h: number
}

function getBetterCardDisposal(width: number, height: number): Resolution {
  const availableResolutions = [
    { w: 6, h: 2 },
    { w: 4, h: 3 },
    { w: 3, h: 4 },
    { w: 2, h: 6 },
  ]
  const boardResolution = width / height

  return availableResolutions.reduce((bestRes, currentRes) => {
    const bestDist = (boardResolution - bestRes.w / bestRes.h) ** 2
    const currentDist = (boardResolution - currentRes.w / currentRes.h) ** 2

    return bestDist < currentDist ? bestRes : currentRes
  })
}

function getCardsBoardLayout(): Rectangle {
  const { width, height } = Storage.gameCanvas.size
  const marginHorizontal = 0.006
  const marginVertical = 0.1

  return {
    x: width * marginHorizontal,
    y: height * marginVertical,
    w: width - width * marginHorizontal * 2,
    h: height - height * marginVertical * 2,
  }
}

function render(): void {
  requestAnimationFrame(drawScreen)
}

function refresh(): void {
  gameState.boardLayout = getCardsBoardLayout()
  gameState.cardsDisposal = getBetterCardDisposal(
    gameState.boardLayout.w,
    gameState.boardLayout.h
  )

  const { cards, setCount } = getBoard()
  gameState.setCount = setCount
  const updateCardInfo = createCardInfoUpdater(cards)
  gameState.cards.forEach(updateCardInfo)

  render()
}

function createCardInfoUpdater(
  cards: Card[]
): (cardInfo: CardInfo, cardIndex: number) => void {
  return (cardInfo, cardIndex) => {
    const { boardLayout, cardsDisposal } = gameState
    const cellW = boardLayout.w / cardsDisposal.w
    const cellH = boardLayout.h / cardsDisposal.h
    const cardMargin = 0.025
    const rectangle = {
      x:
        boardLayout.x +
        ((cardIndex % cardsDisposal.w) * cellW + cellW * cardMargin),
      y:
        boardLayout.y +
        (Math.floor(cardIndex / cardsDisposal.w) * cellH + cellH * cardMargin),
      w: cellW * (1 - cardMargin * 2),
      h: cellH * (1 - cardMargin * 2),
    }

    Object.assign(cardInfo, cards[cardIndex], {
      rectangle,
    })
  }
}

function drawScreen(time: number): void {
  const { buffer, bufferCtx, ctx: canvasCtx } = Storage.gameCanvas
  const { cards, selectedCardIndices } = gameState

  drawBackground(bufferCtx)

  const drawCard = createDrawCard(bufferCtx)
  cards.forEach(drawCard)

  selectedCardIndices.forEach((cardIdx) => {
    drawSelectedFrame(bufferCtx, cards[cardIdx])
  })

  if (canvasCtx) {
    canvasCtx.drawImage(buffer, 0, 0)
  }
}

function drawBackground(ctx: CanvasRenderingContext2D): void {
  const { width, height } = Storage.gameCanvas.size
  ctx.fillStyle = '#002'
  ctx.fillRect(0, 0, width, height)
}

function createDrawCard(
  ctx: CanvasRenderingContext2D
): (card: CardInfo, cardIndex: number) => void {
  return (card, cardIndex): void => {
    ctx.fillStyle = 'snow' // or 'ivory' ?
    ctx.fillRect(
      card.rectangle.x,
      card.rectangle.y,
      card.rectangle.w,
      card.rectangle.h
    )

    drawCardShapes(ctx, card)
  }
}

function drawCardShapes(ctx: CanvasRenderingContext2D, card: CardInfo): void {
  const isVertical = card.rectangle.w < card.rectangle.h
  const cellSize = Math.max(card.rectangle.w, card.rectangle.h) / 3
  const shapeSize = cellSize * 0.8

  // card color
  const color = {
    [CardColor.Green]: '#1C4',
    [CardColor.Red]: '#E52',
    [CardColor.Purple]: '#61E',
  }[card.color]

  ctx.fillStyle = color
  ctx.strokeStyle = color
  ctx.lineWidth = 4

  for (let i = 0; i < card.number; i += 1) {
    const cellW = card.rectangle.w / (isVertical ? 1 : card.number)
    const cellH = card.rectangle.h / (isVertical ? card.number : 1)

    const shapeLayout = {
      x:
        card.rectangle.x +
        (cellW / 2 - shapeSize / 2 + cellW * (isVertical ? 0 : i)),
      y:
        card.rectangle.y +
        (cellH / 2 - shapeSize / 2 + cellH * (isVertical ? i : 0)),
      w: shapeSize,
      h: shapeSize,
    }

    // card shape
    ctx.beginPath()
    switch (card.shape) {
      case CardShape.Squiggle: {
        ctx.moveTo(shapeLayout.x + shapeLayout.w / 2, shapeLayout.y)
        ctx.lineTo(shapeLayout.x + shapeLayout.w, shapeLayout.y + shapeLayout.h)
        ctx.lineTo(shapeLayout.x, shapeLayout.y + shapeLayout.h)
        break
      }
      case CardShape.Oval: {
        ctx.arc(
          shapeLayout.x + shapeLayout.w / 2,
          shapeLayout.y + shapeLayout.h / 2,
          shapeLayout.w / 2,
          0,
          Math.PI * 2
        )
        break
      }
      default:
      case CardShape.Diamond: {
        ctx.rect(shapeLayout.x, shapeLayout.y, shapeLayout.w, shapeLayout.h)
        break
      }
    }
    ctx.closePath()

    // card shading
    switch (card.shading) {
      case CardShading.Solid: {
        ctx.fill()
        break
      }
      case CardShading.Striped: {
        const gradient = ctx.createLinearGradient(
          shapeLayout.x,
          shapeLayout.y,
          shapeLayout.x + shapeLayout.w,
          shapeLayout.y + shapeLayout.h
        )
        gradient.addColorStop(0, color)
        gradient.addColorStop(1 / 4, 'white')
        gradient.addColorStop(2 / 4, color)
        gradient.addColorStop(3 / 4, 'white')
        gradient.addColorStop(1, color)
        ctx.fillStyle = gradient
        ctx.fill()
        break
      }
      default:
        break
    }
    ctx.stroke()
  }
}

function drawSelectedFrame(
  ctx: CanvasRenderingContext2D,
  card: CardInfo
): void {
  ctx.lineWidth = Math.round(
    Math.min(card.rectangle.w, card.rectangle.h) * 0.04
  )
  ctx.strokeStyle = '#FD5'
  ctx.strokeRect(
    card.rectangle.x,
    card.rectangle.y,
    card.rectangle.w,
    card.rectangle.h
  )
}
