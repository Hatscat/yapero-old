export {
  loopIndex,
  removeAtFast,
  lerp,
  isPointInsideRectangle,
  isPointInsideCircle,
  doesCirclesCollides,
  dist2dSq,
  isPointLeftOfLineAB,
}

export type Point2d = {
  x: number
  y: number
}

export type Rectangle = {
  x: number
  y: number
  w: number
  h: number
}

export type Circle = {
  x: number
  y: number
  r: number
}

export type Line = {
  a: Point2d
  b: Point2d
}

function loopIndex(index: number, length: number): number {
  return (length + (index % length)) % length
}

function removeAtFast(array: unknown[], itemIndex: number): void {
  array[itemIndex] = array[array.length - 1]
  array.pop()
}

function lerp(start: number, stop: number, value: number): number {
  return start + (stop - start) * value
}

function isPointInsideRectangle(point: Point2d, rectangle: Rectangle): boolean {
  return (
    point.x >= rectangle.x &&
    point.x <= rectangle.x + rectangle.w &&
    point.y >= rectangle.y &&
    point.y <= rectangle.y + rectangle.h
  )
}

function isPointInsideCircle(point: Point2d, circle: Circle): boolean {
  return (point.x - circle.x) ** 2 + (point.y - circle.y) ** 2 < circle.r ** 2
}

function doesCirclesCollides(circleA: Circle, circleB: Circle): boolean {
  return (
    (circleB.x - circleA.x) ** 2 + (circleB.y - circleA.y) ** 2 <
    (circleB.r + circleA.r) ** 2
  )
}

function dist2dSq(pointA: Point2d, pointB: Point2d): number {
  return (pointA.x - pointB.x) ** 2 + (pointA.y - pointB.y) ** 2
}

function isPointLeftOfLineAB(point: Point2d, line: Line): boolean {
  return (
    (line.b.x - line.a.x) * (point.y - line.a.y) -
      (line.b.y - line.a.y) * (point.x - line.a.x) >
    0
  )
}
